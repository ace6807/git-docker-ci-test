FROM python:2
WORKDIR /code
COPY python-helloworld /code/
CMD ["python", "helloworld/main.py"]